import React, { Component } from 'react';
import io from 'socket.io-client';
import Chat from './Chat'
class App extends Component {
  state = {
    isConnected:false,
    id:null,
    peeps:[],
    answer:"",
    username: 'razan',
    message: 'hii',
messages: [],
old_messages:[]

  }
  socket = null

  componentDidMount(){

    this.socket = io('http://codicoda.com:8000');

    this.socket.on('connect', () => {
      this.setState({isConnected:true})
    })
    this.socket.on('pong!',()=>{
      console.log('the server answered!')
    })
    this.socket.on('pong!',(additionalStuff)=>{
      console.log('server answered!', additionalStuff)
    })
    this.socket.on('youare',(answer)=>{
      this.setState({id:answer.id})
    })
    this.socket.on('room', old_messages => console.log(old_messages))
  
    this.socket.on('disconnect', () => {
      this.setState({isConnected:false})
    })

    /** this will be useful way, way later **/
    this.socket.on('room', old_messages => console.log(old_messages))
    this.socket.on('peeps',(peeps)=>this.setState({peeps}))
    this.socket.on('next',(message_from_server)=>console.log(message_from_server))

    this.sendMessage = ev => {
      ev.preventDefault();
      this.socket.emit('SEND_MESSAGE', {
          author: this.state.username,
          old_messages: this.state.old_messages
      });
      this.setState({old_messages: ''});
  }
  
  }

  componentWillUnmount(){
    this.socket.close()
    this.socket = null
  }
  send = (text) =>{
    const new_message = {
      id:this.state.id,
      name:"Razan",
      text
    }
    this.socket.emit('message',new_message)
    let message = this.state.message
    message = [...message,new_message]
    this.setState({message})

    io.on('connection', (socket) => {
      console.log(socket.id);
  
      socket.on('SEND_MESSAGE', function(data){
          io.emit('RECEIVE_MESSAGE', data);
      })
  });
  }

  render() {
    return (
      <div className="App">
         <div>status: {this.state.isConnected ? 'connected' : 'disconnected'}</div>
         <div>id: {this.state.id}</div>
         <button onClick={()=>this.socket.emit('ping!')}>ping</button>
         <button onClick={()=>this.socket.emit('whoami')}>Who am I?</button>
         {/* <button onClick={()=>this.socket.emit('addition')}>addition</button> */}

        {/* <button onClick={()=>this.socket.emit('give me next')}>give me next</button> */}

         <input type="text" name="answer" onChange={e => this.setState({ answer: e.target.value })} />
        {/* <button onClick={()=>this.socket.emit('answer',this.state.answer)}>Submit the answer</button> */}
        {/* <ul>
          <p>{this.state.peeps.length}  people are connected</p>
          {this.state.peeps.map(peep=><li key={peep}>{peep}</li>)}
        </ul> */}

{/* <div className="messages">
    {this.state.messages.map(message => {
        return (
            <div>{message.author}: {message.message}</div>
        )
    })}
</div> */}
 <button onClick={this.sendMessage} className="btn btn-primary form-control">Send</button> 


      </div>
    );
  }
}

export default App;
